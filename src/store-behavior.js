window.LittleReads = window.LittleReads || {};

// action types
const LOAD = "LOAD";
const USERLOGIN = "USERLOGIN";
const USERSIGNUP = "USERSIGNUP";
const LOGGEDIN = "LOGGEDIN";
const OFFLINE = "OFFLINE";
const ONLINE = "ONLINE";
const USERLOGOUT = "USERLOGOUT";
const ADDBOOK = "ADDBOOK";
const DELETEBOOK = "DELETEBOOK";
const CHANGERATING = "CHANGERATING";
const CHANGETIMESREAD = "CHANGETIMESREAD";
// const SELECT_USER = "SELECT_USER";
const UPDATESTOREUSERLISTS = "UPDATESTOREUSERLISTS";
const UPDATEUIDSUSERLISTS = "UPDATEUIDSUSERLISTS";


//action creator
LittleReads.act = {
	load: function(data) {
		return {
			type: LOAD,
			data: data
		};
	},
	userlogin: function(username, email, password){
		return {
			type: USERLOGIN,
			username: username,
			email: email,
			password: password
		};
	},
	usersignup: function(username, email, password){
		return{
			type: USERSIGNUP,
			username: username,
			email: email,
			password: password
		};
	},
	loggedin: function(uid, username, email, login, create, loggedIn){
		return{
			type: LOGGEDIN,
			uid: uid,
			username: username,
			email: email,
			login: login,
			create: create,
			loggedIn: loggedIn
		};
	},
	offline: function(uid, username, email, loggedIn, online){
		return{
			type: OFFLINE,
			uid: uid,
			username: username,
			email: email,
			loggedIn: loggedIn,
			online: online
		};
	},
	online: function(online){
		return{
			type: ONLINE,
			online: online
		};
	},
	userlogout: function(){
		return{
			type: USERLOGOUT
		};
	},
	updateStoreUserLists: function(username, userlistcontent){
		return{
			type: UPDATESTOREUSERLISTS,
			username: username,
			userlistcontent: userlistcontent
		};
	},
	updateUidsUserLists: function(username, userlistcontent){
		return{
			type: UPDATEUIDSUSERLISTS,
			username: username,
			userlistcontent: userlistcontent
		};
	},
	// selectUser: function(id){
	// 	return{
	// 		type: SELECT_USER,
	// 		id: id
	// 	};
	// },
	addbook: function(newBook){
		return{
			type: ADDBOOK,
			// userid: userid,
			newbook: newBook
		};
	},
	deletebook: function(title){
		return{
			type: DELETEBOOK,
			title: title
		};
	},
	changerating: function(title, rater, newvalue, updated){
		return{
			type: CHANGERATING,
			// userid: userid,
			title: title,
			rater: rater,
			newvalue: newvalue,
			updated:updated
		}
	},
	changetimesread: function(title, date, action){
		return {
			type: CHANGETIMESREAD,
			// userid: userid,
			title: title,
			date: date,
			action: action
		};
	},
}

const emptyState = {
	user: {
		"username": "",
		"uid": "",
		"email": "",
		"password": "",
		"hasMoreAccounts": false,
		"login": false,
		"create": false,
		"loggedIn": false,
		"online" : true
	},
	selectedListId: "",
	userLists: {},
};

function reducer(state, action){
	if (!state) return emptyState;
	var userId, title, date, bookindex, rater, newValue;
	var userList, userBooksList, ind;
	var newState = {};
	var updateFirebase = false;
	var username;

	switch(action.type){
		case LOAD:
			// return action.data;
			newState = action.data;
			break;
	  // case SELECT_USER:
	  	// TODO
	  case USERLOGIN:
		  	username = action.username.toLowerCase();
		  	email = action.email;
		  	password = action.password;
		  	var updatedUser = updateUser(state, {
		  		'username': username,
		  		'email' : email,
		  		'password' : password,
		  		'login' : true
		  	});
		  	// return Object.assign({}, state, {user: updatedUser});
		  	newState = Object.assign({}, state, {user: updatedUser});
		  	break;
	  case USERSIGNUP:
	  	username = action.username.toLowerCase();
	  	email = action.email;
	  	password = action.password;
	  	//console.log('in usersignup reducer');
	  	var updatedUser = updateUser(state, {
	  		'username': username,
	  		'email' : email,
	  		'password' : password,
	  		'create' : true
	  	});
	  	// return Object.assign({}, state, {user: updatedUser});
	  	newState = Object.assign({}, state, {user: updatedUser});
	  	break;
	  case LOGGEDIN:
	  	uid = action.uid;
	  	username = action.username.toLowerCase();
	  	email = action.email;
	  	login = action.login;
	  	create = action.create;
	  	loggedIn = action.loggedIn;
	  	var updatedUser = updateUser(state, {
	  		'uid': uid,
	  		'username': username,
	  		'email': email,
	  		'login': login,
	  		'create': create,
	  		'loggedIn': loggedIn
	  	});
	  	// return Object.assign({}, state, {user: updatedUser});
	  	newState = Object.assign({}, state, {user: updatedUser});
	  	break;
	  case OFFLINE:
	  	uid = action.uid;
	  	username = action.username.toLowerCase();
	  	email = action.email;
	  	loggedIn = action.loggedIn;
	  	online = action.online;
	  	var updatedUser = updateUser(state, {
	  		'uid': uid,
	  		'username': username,
	  		'email': email,
	  		'loggedIn': loggedIn,
	  		'online': online
	  	});
	  	// return Object.assign({}, state, {user: updatedUser});
	  	newState = Object.assign({}, state, {user: updatedUser});
	  	break;
	  case ONLINE:
	  	online = action.online;
	  	var updatedUser = updateUser(state, {
	  		'online': online
	  	});
	  	// return Object.assign({}, state, {user: updatedUser});
	  	newState = Object.assign({}, state, {user: updatedUser});
	  	break;
	  case USERLOGOUT:
	  	// console.log('emptystate user is', emptyState.user);
	  	// console.log('need to reset store to empty state');
	  	newState = Object.assign({}, state, {user: emptyState.user, userLists: emptyState.userLists});
	  	break;
	  case UPDATESTOREUSERLISTS:
	  	//does not update firebase
	  	// console.log("in updatestore reducer");
	  	// username = action.username.toLowerCase();
			userlistcontent = action.userlistcontent;
			newState = Object.assign({}, state, {userLists: userlistcontent});
			// console.log('newState is ', newState);
			break;
		case UPDATEUIDSUSERLISTS:
	  	updateFirebase = true;
	  	username = action.username.toLowerCase();
			userlistcontent = action.userlistcontent;
			newState = Object.assign({}, state, {userLists: userlistcontent});
			console.log('in updateuidsuserlists newState is ', newState);
			break;
	  case ADDBOOK:
	  	var newBook = action.newbook;
	  	updateFirebase = true;
	  	userList = state.userLists;
	  	if (!userList) {
	  		// return state;
	  		newState = state;
	  		break;
	  	}
	  	userBooksList = userList.booksList;
	  	if (userBooksList){
	  		bookindex = findMatchingTitle(userBooksList,newBook.title);
	  		if (bookindex > -1){
	  			console.log("Book with the same title already exists.");
	  			// return state;
	  			newState = state;
	  		} else {
	  			// console.log("adding book");
	  			var updatedBooksList =  updateBooksList(state,"add",newBook);
	  			userUpdateHash = {
	  				'booksList' : updatedBooksList
	  			};
	  			// var updatedCurrentUser = updateCurrentUser(state, userUpdateHash);
	  			// var updatedUserLists = updateUserLists(state, updatedCurrentUser);
	  			var updatedUserLists = updateUserLists(state, userUpdateHash);
	  			newState = Object.assign({}, state, {userLists: updatedUserLists});
	  		}
	  	} else {
	  		userUpdateHash = {
	  			'booksList' : [newBook]
	  		};
	  		var updatedUserLists = updateUserLists(state, userUpdateHash);
	  		newState = Object.assign({}, state, {userLists: updatedUserLists});
	  	}
	  	break;
	  case DELETEBOOK:
	  	var title = action.title;
	  	updateFirebase = true;
	  	userList = state.userLists;
	  	if (!userList) {
	  		newState = state;
	  		break;
	  	}
	  	userBooksList = userList.booksList;
	  	var bookindex = findMatchingTitle(userBooksList,title);
	  	if (bookindex === -1){
	  		console.log("Book not found");
	  		newState = state;
	  	} else {
	  		var totalReadCopy = userList.totalRead;
	  		var readEntries = userBooksList[bookindex].read.length;
	  		totalReadCopy -= (readEntries-1); // first entry is 0
	  		// find total number of entries in readlog
	  		// delete totalRead by that number
	  		var updatedBooksList =  updateBooksList(state,"delete",title,bookindex);
	  		userUpdateHash = {
	  			'booksList' : updatedBooksList,
	  			'totalRead' : totalReadCopy
	  		};
	  		// var updatedCurrentUser = updateCurrentUser(state, userUpdateHash);
	  		// var updatedUserLists = updateUserLists(state, updatedCurrentUser);
	  		var updatedUserLists = updateUserLists(state, userUpdateHash);
	  		newState = Object.assign({}, state, {userLists: updatedUserLists});
	  	}
	  	break;
	  case CHANGERATING:
	  	updateFirebase = true;
	  	// userId = action.userid;
			title = action.title;
			rater = action.rater;
			newValue = action.newvalue;
			updated = action.updated;
			// userList = state.userLists[userId];
			userList = state.userLists;
	  	if (!userList) {
	  		// return state;
	  		newState = state;
	  		break;
	  	}
	  	userBooksList = userList.booksList;
	  	// console.log(userBooksList);
	  	bookindex = findMatchingTitle(userBooksList,title);
	  	// console.log(bookindex);
	  	if (bookindex > -1){
	  		var bookUpdateHash = {};
	  		bookUpdateHash[rater] = newValue;
	  		bookUpdateHash["updated"]=updated;
	  		// console.log(bookUpdateHash);
	  		// var updatedCurrentBook = updateBook(state, userId, bookindex, bookUpdateHash);
	  		var updatedCurrentBook = updateBook(state, bookindex, bookUpdateHash);
	  		// var updatedBooksList =  updateBooksList(state,userId,bookindex,updatedCurrentBook);
	  		var updatedBooksList =  updateBooksList(state,'update',updatedCurrentBook,bookindex);
	  		userUpdateHash = {
	  			'booksList' : updatedBooksList
	  		};
	  		// var updatedCurrentUser = updateCurrentUser(state, userId, userUpdateHash);
	  		// var updatedCurrentUser = updateCurrentUser(state, userUpdateHash);
	  		// var updatedUserLists = updateUserLists(state, userId, updatedCurrentUser);
	  		// var updatedUserLists = updateUserLists(state, updatedCurrentUser);
	  		var updatedUserLists = updateUserLists(state, userUpdateHash);
	  		// return Object.assign({}, state, {userLists: updatedUserLists});
	  		newState = Object.assign({}, state, {userLists: updatedUserLists});
	  	} else {
	  		console.log('book not found');
	  		// return state;
	  		newState = state;
	  	}
	  	break;
	  case CHANGETIMESREAD:
	  	var title, newReadEntry,readLogCopy, totalReadCopy;
	  	var times = 0;
	  	updateFirebase = true;
	  	userList = state.userLists;
	  	if (!userList){ 	// return state;
	  		newState = state;
	  		break;
	  	}
	  	title = action.title;
	  	date = action.date;
	  	action = action.action;
	  	userBooksList = userList.booksList;
	  	bookindex = findMatchingTitle(userBooksList,title);
	  	totalReadCopy = userList.totalRead;

	  	if ((bookindex > -1) && !((totalReadCopy === 0) && (action==="decrement"))){ // book existr
	  		if (action === "increment"){
	  			times = 1;
	  			newReadEntry = {'date':date, 'times': times};
	  			if (userBooksList[bookindex].read){ //only if the title exists make a clone of readLog and update it
	  				readLogCopy = userBooksList[bookindex].read.slice(0);
	  				readLogCopy.push(newReadEntry);
	  			} else {
	  				readLogCopy = [newReadEntry];
	  			}
	  			totalReadCopy += times;
	  		} else if (action === "decrement"){
	  			times = -1;
	  			if (userBooksList[bookindex].read){ //only if the title exists make a clone of readLog and remove the last entry
	  				if (userBooksList[bookindex].read.length > 1){
	  					var orcopy = userBooksList[bookindex].read.slice(0);
	  					var readLogCopy = orcopy.slice(0,-1);
	  					totalReadCopy += times;
	  				}	 else { // make no changes
	  					return state;
	  				}
	  			}
	  		}
	  		var bookUpdateHash = {
	  			"read" : readLogCopy,
	  			"updated": date
	  		};

	  		var updatedCurrentBook = updateBook(state, bookindex, bookUpdateHash);
	  		var updatedBooksList =  updateBooksList(state,"update",updatedCurrentBook,bookindex);
	  		userUpdateHash = {
	  			'totalRead' : totalReadCopy,
	  			'booksList' : updatedBooksList
	  		};
	  		var updatedUserLists = updateUserLists(state, userUpdateHash);
	  		newState = Object.assign({}, state, {userLists: updatedUserLists});
	 		} else {
	  		console.log("Store::CHANGETIMESREAD Book Title not found. or totalread is zero and you wanted to decrement");
	  		newState = state;
	  	}
	  	break;
		default:
			// return state;
			newState = state;
	}
	// console.log('updated store', newState);
	//call firebase to update with the new store
	if (updateFirebase === true){
		firebaseUpdate(newState, state, username);
	}
	// console.log(newState);
	return newState;
}

// function rawUserListsSelector(state){
// 	return state.userLists;
// }

// function selectedUserIdSelector(state){
// 	return state.selectedUserId;
// }
function firebaseUpdate(newState,state, username){
	// console.log('in firebaseupdate newState is', newState);
	// console.log('state is', state);
	// console.log('username is', username);
	this.doc.path = null;
	// this.doc[0].path = null;
	var  keys = Object.keys(newState.userLists);
	//this.doc.data
	var str = 'users/'+newState.user.username+'/userLists/';
	// console.log(str);
	this.doc.setStoredValue(str, newState.userLists)
	// this.doc[0].setStoredValue(str, newState.userLists)
		.then(function(response){
			// console.log('done setting firebase store');
		})
		.catch(function(error){
			console.log('error in firebaseUpdate');
	});
	//username is in 'users/'+newState.user.username+'/'
	// console.log(this);
}
function updateUser(state,  updateHash){
	var currentUser = Object.assign({}, state.user);
	var currentUserKeys = Object.keys(currentUser);
	for (var key in updateHash){
		if (currentUserKeys.indexOf(key) > -1){
			currentUser[key] = updateHash[key];
		}
	}
	return currentUser;
}

function updateUserLists(state, updateHash){
	// make a copy of the entire current userlist object
	var currentUserCopy = Object.assign({}, state.userLists);
	var currentUserKeys = Object.keys(currentUserCopy);
	for (var key in updateHash){
		// if the key of updated hash is indeed in the userkeys
		if ((currentUserKeys.indexOf(key) > -1) || (key === 'booksList')){
			currentUserCopy[key] = updateHash[key];
		}
	}
	return currentUserCopy;
}

// function updateUserLists(state, updatedCurrentUser ){
// 	// copy the userLists of all users
// 	// console.log(updatedCurrentUser);
// 	var userListsCopy = Object.assign({}, state.userLists);
// 	// update the current user list
// 	//
// 	userListsCopy = updatedCurrentUser;
// 	return userListsCopy;
// }

function updateBook(state, currentBookIndex, updateHash){
	// make a copy of the current book
	// var currentBookCopy = Object.assign({}, state.userLists[userId].booksList[currentBookIndex]);
	var currentBookCopy = Object.assign({}, state.userLists.booksList[currentBookIndex]);
	var currentBookKeys = Object.keys(currentBookCopy);
	for (var key in updateHash){
		// if (currentBookKeys.indexOf(key) > -1){
			currentBookCopy[key] = updateHash[key];
		// }
	}
	return currentBookCopy;
}

function updateBooksList(state, action, currentBookCopy, currentBookIndex){
	// get the booksList of the current user
	// var userBooksList = state.userLists[userId].booksList;
	var userBooksList = state.userLists.booksList;
	// copy the booksList of the current user into a new arry
	var userBooksListCopy = userBooksList.slice(0);
	if (action === 'update'){
		//existing book
		// update the current user list
		userBooksListCopy[currentBookIndex] = currentBookCopy;
		return userBooksListCopy;
	} else if (action === "add"){
		// console.log("need to add book");
		userBooksListCopy.push(currentBookCopy);
		return userBooksListCopy;
	} else if (action === "delete"){
		// remove the book with the bookindex
	  var deletedObj = userBooksListCopy.splice(currentBookIndex,1);
	  // console.log(userBooksListCopy);
		return userBooksListCopy;
	}
}

function findMatchingTitle(booksList,title){
	var bookTitles = [];
	if (booksList.length > 0){
		booksList.filter(function(book){
			var booktitle = book.title.toLowerCase();
		  bookTitles.push(booktitle);
		});
		var smalltitle = title.toLowerCase();
		return bookTitles.indexOf(smalltitle);
	}
	return 0;
}


// LittleReads.select = {
// 	selectedListIds: selectedListIdsSelector
// }
// 	selectedUserList: selectedUserListSelector
//
const store = Redux.createStore(reducer, window.devToolsExtension && window.devToolsExtension());
LittleReads.store = store;

// selectors are used to create derived data from the store
